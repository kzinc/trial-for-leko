const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = process.env.PORT || 5000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

let INCREMENTAL_ID = 5;

const gameOfThronesDataset = {
  1: {
    name: 'Waymar Royce',
    whois: 'Night Watch member',
    deathReason: 'stabbed with a sword',
    killer: 'White Walker',
    weapon: 'Ice Sword',
  },
  2: {
    name: 'Gared',
    whois: 'Night Watch member',
    deathReason: 'stabbed with a sword',
    killer: 'White Walker',
    weapon: 'Ice Sword',
  },
  3: {
    name: 'Will',
    whois: 'Night Watch member',
    deathReason: 'stabbed with a sword',
    killer: 'Ned Stark',
    weapon: 'Sword',
  },
  4: {
    name: 'Incredibly long name for testing name',
    whois: 'Incredibly long name for testing Night Watch member',
    deathReason: ' Incredibly long name for testing stabbed with a sword',
    killer: 'Incredibly long name for testing Ned Stark',
    weapon: 'Incredibly long name for testing Sword',
  },
};

const getFilteredData = (filter) => {
  const result = {};
  Object.keys(gameOfThronesDataset).forEach((hero) => {
    const heroPropsString = JSON.stringify(gameOfThronesDataset[hero]);
    if (heroPropsString.toLowerCase().includes(filter.toLowerCase())) {
      result[hero] = gameOfThronesDataset[hero];
    }
  });
  return result;
};

app.get('/api/gameOfThronesDataset', (req, res) => {
  res.send(JSON.stringify(gameOfThronesDataset));
});

app.post('/api/editDatasetRecord', ({ body }, res) => {
  const { id, newValues } = body.post;
  Object.keys(newValues).foreach((field) => {
    gameOfThronesDataset[id][field] = newValues[field];
  });
  res.send(gameOfThronesDataset[id]);
});

app.post('/api/getFilteredData', ({ body }, res) => {
  const { post } = body;
  const result = getFilteredData(post);
  res.send(JSON.stringify(result));
});

app.post('/api/deleteHero', ({ body }, res) => {
  const { post } = body;
  delete (gameOfThronesDataset[post]);
  res.send(JSON.stringify(true));
});

app.post('/api/getHeroData', ({ body }, res) => {
  const { post } = body;
  res.send(JSON.stringify(gameOfThronesDataset[post]));
});

app.post('/api/saveHero', ({ body }, res) => {
  const { hero, heroInfo } = body.post;
  const hasEmptyValue = () => Object.values(heroInfo).includes('');
  if (hasEmptyValue) res.send(JSON.stringify({ error: 'Недопустимо пустое значение параметра персонажа' }));
  if (gameOfThronesDataset[hero]) gameOfThronesDataset[hero] = heroInfo;
  if (!hero) {
    gameOfThronesDataset[INCREMENTAL_ID] = heroInfo;
    INCREMENTAL_ID++;
  }
  res.send(JSON.stringify({ success: 'Данные успешно обновлены' }));
});


// eslint-disable-next-line no-console
app.listen(port, () => console.log(`Listening on port ${port}`));
