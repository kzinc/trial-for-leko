import React, { Component } from 'react';
import { getRequest, postRequest } from '../../helpers/apiRequests/apiRequest';
import Header from '../../components/Header/Header';
import Table from '../../components/Table/Table';
import styles from './MainPage.module.css';
import Modal from '../../components/Modals/Modal';
import EditCard from '../../components/EditCard/EditCard';
import Input from '../../components/Input/Input';
import Button from '../../components/Buttons/Button';

class MainPage extends Component {
  delayTimer;

  state = {
    dataSet: {},
    displayType: 'table',
    edit: null,
    add: false,
    searchValue: '',
  };

  componentDidMount() {
    this.getFullDataset();
  }

  getFullDataset = () => getRequest('gameOfThronesDataset').then((dataSet) => this.setState({ dataSet }));

  handleSearch = async (searchValue) => {
    if (searchValue === '') this.getFullDataset();
    this.setState({ searchValue });
    clearTimeout(this.delayTimer);
    this.delayTimer = setTimeout(async () => {
      const dataSet = await postRequest('getFilteredData', searchValue);
      this.setState({ dataSet });
    }, 1000);
  }

  editHero = (heroId) => {
    this.setState({ edit: heroId });
  }

  deleteHero = async (heroId) => {
    await postRequest('deleteHero', heroId);
    this.getFullDataset();
  }

  switchDisplayType = (type) => {
    const displayType = type === 0 ? 'table' : 'grid';
    this.setState({ displayType });
  }

  onCloseEditModal = (shouldUpdateList) => {
    const { searchValue } = this.state;
    this.setState({ edit: 0, add: false });
    if (shouldUpdateList) this.handleSearch(searchValue);
  }

  render() {
    const {
      add, dataSet, displayType, edit, searchValue,
    } = this.state;
    return (
      <div className={styles.container}>
        {edit || add ? (
          <Modal onClose={(update) => this.onCloseEditModal(update)}>
            <EditCard hero={+edit} onClose={(update) => this.onCloseEditModal(update)} />
          </Modal>
        ) : null}
        <Header onSelect={(type) => this.switchDisplayType(type)} displayType={displayType === 'table' ? 0 : 1} />
        <div className={styles.inputContainer}>
          <Button action={() => this.setState({ add: true })} color="blue">Добавить</Button>
          <Input
            value={searchValue}
            valid
            onChange={(value) => this.handleSearch(value)}
            onSubmit={() => this.handleSearch(searchValue)}
          />
        </div>
        {
          Object.keys(dataSet).length
            ? (
              <Table
                data={dataSet}
                editHero={(heroId) => this.editHero(heroId)}
                deleteHero={(heroId) => this.deleteHero(heroId)}
              />
            )
            : (
              <div className={styles.nothingToDisplay}>
                Нет элементов для отображения, измените фильтр,
                или добавьте новый элемент самостоятельно
              </div>
            )
        }
      </div>
    );
  }
}
export default MainPage;
