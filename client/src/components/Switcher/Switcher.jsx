import React from 'react';
import PropTypes from 'prop-types';
import styles from './Switcher.module.css';

const Switcher = ({ items, onSelect, selected }) => {
  const handleKeyPress = (e) => {
    if (e.charCode === 32) {
      if (items[selected + 1]) {
        onSelect(selected + 1);
      } else {
        onSelect(0);
      }
    }
  };

  const getClassName = (itemId) => (selected === itemId ? styles.itemSelected : styles.item);

  const renderItem = (item, itemId) => {
    return (
      <div
        key={item}
        className={getClassName(itemId)}
        role="button"
        onKeyPress={(e) => handleKeyPress(e)}
        tabIndex={itemId}
        onClick={() => onSelect(itemId)}
      >
        {item}
      </div>
    );
  };
  return (
    <div className={styles.switcherContainer}>
      {items.map((item, i) => renderItem(item, i))}
    </div>
  );
};


Switcher.propTypes = {
  onSelect: PropTypes.func.isRequired,
  items: PropTypes.arrayOf(PropTypes.string).isRequired,
  selected: PropTypes.number.isRequired,
};

export default Switcher;
