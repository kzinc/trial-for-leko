import React from 'react';
import PropTypes from 'prop-types';
import styles from './Header.module.css';
import Switcher from '../Switcher/Switcher';

const Header = ({ onSelect, displayType }) => {
  return (
    <div className={styles.headerContainer}>
      <div className={styles.titleText}>
        Интерфейс просмотра и управления данными по смертям в сериале Игра престолов
      </div>
      <Switcher items={['Таблица', 'Грид']} onSelect={onSelect} selected={displayType} />
    </div>
  );
};

Header.propTypes = {
  onSelect: PropTypes.func.isRequired,
  displayType: PropTypes.number.isRequired,
};

export default Header;
