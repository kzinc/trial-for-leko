import React from 'react';
import PropTypes from 'prop-types';
import styles from './Table.module.css';

const Table = ({ data, editHero, deleteHero }) => {
  const handleKeyPress = (e, action, id) => {
    if (e.charCode === 32) {
      if (action === 'edit') {
        editHero(id);
      } else {
        deleteHero(id);
      }
    }
  };

  const renderTableHeader = () => {
    return (
      <div className={styles.tableHeader}>
        <div className={styles.id} />
        <div className={styles.name}> Имя </div>
        <div className={styles.whois}> Описание </div>
        <div className={styles.deathReason}> Причина смерти </div>
        <div className={styles.killer}> Убийца </div>
        <div className={styles.weapon}> Оружие убийства </div>
        <div className={styles.lastHeaderBlock} />
      </div>
    );
  };

  const renderRow = (heroId) => {
    const {
      name, whois, deathReason, killer, weapon,
    } = data[heroId];
    return (
      <div className={styles.tableRow} key={heroId}>
        <div className={styles.id}>{`#${heroId}`}</div>
        <div className={styles.name}>{name}</div>
        <div className={styles.whois}>{whois}</div>
        <div className={styles.deathReason}>{deathReason}</div>
        <div className={styles.killer}>{killer}</div>
        <div className={styles.weapon}>{weapon}</div>
        <div
          className={styles.edit}
          label="edit"
          onClick={() => editHero(heroId)}
          role="button"
          onKeyPress={(e) => handleKeyPress(e, 'edit', heroId)}
          tabIndex={heroId}
        />
        <div
          className={styles.delete}
          label="delete"
          onClick={() => deleteHero(heroId)}
          role="button"
          onKeyPress={(e) => handleKeyPress(e, 'delete', heroId)}
          tabIndex={heroId}
        />
      </div>
    );
  };
  return (
    <div className={styles.tableContainer}>
      {renderTableHeader()}
      {Object.keys(data).map((row) => renderRow(row))}
    </div>
  );
};

Table.propTypes = {
  data: PropTypes.shape({
    id: PropTypes.shape({
      name: PropTypes.string,
      whois: PropTypes.string,
      deathReason: PropTypes.string,
      killer: PropTypes.string,
      weapon: PropTypes.string,
    }),
  }).isRequired,
  editHero: PropTypes.func.isRequired,
  deleteHero: PropTypes.func.isRequired,
};

export default Table;
