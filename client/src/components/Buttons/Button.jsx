import React from 'react';
import PropTypes from 'prop-types';
import styles from './Button.module.css';

const Button = ({
  action, children, color, inactive,
}) => {
  const handleKeyPress = ({ charCode }) => {
    if (charCode === 32 && !inactive) {
      action();
    }
  };

  const backgroundColor = `var(--${inactive ? 'grey' : color})`;

  return (
    <div
      className={styles.buttonContainer}
      onClick={() => (inactive ? null : action())}
      label="button"
      role="button"
      onKeyPress={(e) => handleKeyPress(e)}
      tabIndex="12"
      style={{ backgroundColor }}
    >
      {children}
    </div>
  );
};

Button.propTypes = {
  children: PropTypes.node.isRequired,
  action: PropTypes.func.isRequired,
  color: PropTypes.oneOf(['red', 'blue']).isRequired,
  inactive: PropTypes.bool.isRequired,
};

export default Button;
