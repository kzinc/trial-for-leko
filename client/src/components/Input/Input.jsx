import React from 'react';
import PropTypes from 'prop-types';
import styles from './Input.module.css';

const Input = ({
  onChange, onSubmit, value, valid,
}) => {
  const handleKeyPress = ({ charCode }) => {
    if (charCode === 13) {
      onSubmit();
    }
  };

  const getClassName = () => (valid ? styles.inputContainer : styles.inputContainerInvalid);

  return (
    <div className={getClassName()}>
      <input
        type="text"
        name="search"
        value={value}
        onChange={(e) => onChange(e.target.value)}
        onKeyPress={(e) => handleKeyPress(e)}
      />
      <div
        className={styles.clear}
        label="close"
        onClick={() => onChange('')}
        role="button"
        onKeyPress={() => onChange('')}
        tabIndex="1"
      />
    </div>
  );
};

Input.propTypes = {
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  valid: PropTypes.bool.isRequired,
};

export default Input;
