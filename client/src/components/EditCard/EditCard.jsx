import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import styles from './EditCard.module.css';
import { postRequest } from '../../helpers/apiRequests/apiRequest';
import Button from '../Buttons/Button';
import Input from '../Input/Input';

const initialHeroState = {
  name: '',
  whois: '',
  deathReason: '',
  killer: '',
  weapon: '',
};

const EditCard = ({ hero, onClose }) => {
  const [heroInfo, setHeroInfo] = useState(initialHeroState);

  useEffect(() => {
    const fetchData = async () => {
      const result = await postRequest('getHeroData', hero);
      setHeroInfo(result);
    };
    fetchData();
  }, [hero]);

  const changeField = (value, prop) => {
    const newHero = { ...heroInfo };
    newHero[prop] = value;
    setHeroInfo(newHero);
  };

  const saveHero = async () => {
    await postRequest('saveHero', { hero, heroInfo });
    onClose(true);
  };

  const renderInputs = () => {
    return [
      { label: 'Имя', prop: 'name' },
      { label: 'Описание', prop: 'whois' },
      { label: 'Причина смерти', prop: 'deathReason' },
      { label: 'Убийца', prop: 'killer' },
      { label: 'Оружие убийства', prop: 'weapon' },
    ].map(({ label, prop }) => {
      return (
        <div key={label} className={styles.row}>
          <div className={styles.label}>{label}</div>
          <Input
            value={heroInfo[prop]}
            valid={!!heroInfo[prop]}
            onChange={(value) => changeField(value, prop)}
            onSubmit={() => changeField(prop)}
          />
        </div>
      );
    });
  };

  const hasEmptyValue = () => Object.values(heroInfo).includes('');

  return (
    <div className={styles.cardContainer}>
      <div className={styles.cardHeader}>
        {hero ? 'Редактирование персонажа' : 'Добавление персонажа'}
      </div>
      <div className={styles.inputsContainer}>
        {renderInputs()}
      </div>
      <div className={styles.buttonsContainer}>
        <Button action={() => saveHero()} color="blue" inactive={hasEmptyValue()}>Сохранить</Button>
        <Button action={() => onClose(false)} inactive={false} color="red">Закрыть</Button>
      </div>
    </div>
  );
};

EditCard.propTypes = {
  hero: PropTypes.number.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default EditCard;
