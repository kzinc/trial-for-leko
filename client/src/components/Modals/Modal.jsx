import React from 'react';
import PropTypes from 'prop-types';
import styles from './Modal.module.css';

const Modal = ({ children, onClose }) => {
  const handleKeyPress = ({ charCode }) => {
    if (charCode === 32) {
      onClose();
    }
  };

  return (
    <div className={styles.modalContainer}>
      {children}
      <div
        className={styles.close}
        label="close"
        onClick={() => onClose()}
        role="button"
        onKeyPress={(e) => handleKeyPress(e)}
        tabIndex="1"
      />
    </div>
  );
};

Modal.propTypes = {
  children: PropTypes.node.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default Modal;
