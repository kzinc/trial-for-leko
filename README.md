** Чтобы потестировать проект клонируйте его и запустите сервер**

*Если не установлен nodemon, то необходимо его установить перед запуском*

`npm install -g nodemon`

*После чего можно разворачивать проект*

 `git clone https://kzinc@bitbucket.org/kzinc/trial-for-leko.git && cd trial-for-leko && npm install && cd client && npm install && cd ../ && npm run dev`